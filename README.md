# Automated Network Deployment: Multicloud VPN - GCP-AWS VPN

This has been created as part of the Velostrata GCP lab for Cloudreachers. Use with caution!

Demonstration of Terraform for automated deployment of network infrastructure in
both Google Cloud Platform (GCP) and Amazon Web Services (AWS). This is a
multi-cloud VPN setup.

You can look at an [architecture diagram for this setup
here](images/autonetdeploy_gcpawsvpn_arch.png).

## Quick Start

*   Install Terraform: ./get_terraform.sh
    *   export PATH=${HOME}/terraform:$ {PATH}
*   Setup for AWS.
    *   Using the three colon menu in Cloud Shell, upload your access keys for AWS to the home directory.
    *   ./aws_set_credentials.sh ~/accessKeys.csv
*   Setup for GCP.
    *   Upload your service account management JSON key to the Cloud Shell, under home directory.
    *   ./gcp_set_credentials.sh ~/<key_file>.json
    *   gcloud config set project [YOUR-PROJECT_ID]
    *   ./gcp_set_project.sh (will make the project ID as a variable in your .tfvars file)
*   Setup variables file.
    *   cd terraform/
    *   vi terraform.tfvars
    *   set the google_compute_network_name = "Name of GCP network"
    *   set the google_compute_subnet_name  = "Name of GCP subnet", where the target test GCP vm will live.
    *   set the aws_vpc_id = "AWS VPC ID", where the source to-be-migrated VMs live.
    *   set the aws_subnet_id = "AWS SUBNET ID", where the test AWS vm will live.
*   Run Terraform.
    *   Examine configuration files.
    *   terraform init
    *   terraform validate
    *   If you have an Internet Gateway already attached to your VPC, then please comment the "aws_internet_gateway" resource under aws_networking.tf. Then, put in your IGW ID under gateway_id attribute of "aws_default_route_table" resource. If you do NOT have an Internet Gateway, please skip this step.
    *   terraform plan
    *   terraform apply
    *   terraform output
    *   gcloud compute instances list
    *   ssh -i ~/.ssh/vm-ssh-key [GCP_EXTERNAL_IP]
    *   ping -c 5 google.com
    *   curl ifconfig.co/ip
    *   Run iperf over external route: /tmp/run_iperf_to_ext.sh
    *   Run iperf over VPN route: /tmp/run_iperf_to_int.sh
    *   exit
    *   ssh -i ~/.ssh/vm-ssh-key [AWS_EXTERNAL_IP]
    *   ping -c 5 google.com
    *   curl ifconfig.co/ip
    *   Run iperf over external route: /tmp/run_iperf_to_ext.sh
    *   Run iperf over VPN route: /tmp/run_iperf_to_int.sh
    *   exit

## References

*   [GCP Cloud VPN Overview](https://cloud.google.com/compute/docs/vpn/overview)
*   [GCP Creating a
    VPN](https://cloud.google.com/compute/docs/vpn/creating-vpns)
*   [GCP VPN Interoperability
    Guides](https://cloud.google.com/compute/docs/vpn/interop-guides)
*   [AWS VPN
    Connections](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/vpn-connections.html)

## License

Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
